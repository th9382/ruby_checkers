require_relative './board.rb'
require_relative './player.rb'

class Game
  attr_reader :board

  def initialize
    @board = Board.new
    @players = { red: Player.new(:red),
                 black: Player.new(:black)
               }

    @current_player = :red
  end

  def play
    until board.over?
      @players[@current_player].play_turn(board)

      @current_player = ((@current_player == :red) ? :black : :red)
    end

    puts "Game Over. #{board.winner} won!"
  end
end
