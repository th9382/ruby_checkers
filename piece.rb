# encoding: utf-8
require 'colorize'
require_relative './exceptions.rb'

class Piece
  RED_MOVES = [[1, -1], [1, 1]]
  BLACK_MOVES = [[-1, -1], [-1, 1]]
  COLORS = [:red, :black]

  SYMBOLS = {
              red: { regular: '◎'.colorize(:red), king: '●'.colorize(:red) },
              black: { regular: '◎', king: '◌'}
            }

  attr_reader :color
  attr_accessor :pos

  def initialize(color, pos, board)
    raise ArgumentError.new("Invalid color") unless COLORS.include?(color)

    @color = color
    @pos = pos
    @board = board
    @king = false

    @board.add_piece(pos, self)
  end

  def render
    (crowned? ? SYMBOLS[color][:king] : SYMBOLS[color][:regular]).encode('utf-8')
  end

  def crowned?
    @king
  end

  def crowned_row
    (color == :red) ? (@board.size - 1) : 0
  end

  def move_diffs
    unless crowned?
      (color == :red) ? RED_MOVES : BLACK_MOVES
    else
      BLACK_MOVES + RED_MOVES
    end
  end

  def slide_moves
    x, y = pos

    moves = []

    move_diffs.each do |dx, dy|
      new_pos = [x + dx, y + dy]
      next unless @board.valid_pos?(new_pos)
      next unless @board.empty?(new_pos)
      moves << new_pos
    end

    moves
  end

  def jump_moves
    x, y = pos

    moves = []

    move_diffs.each do |dx, dy|
      jump_pos = [x + dx, y + dy]

      next unless @board.valid_pos?(jump_pos)
      next if @board.empty?(jump_pos)
      next if @board[jump_pos].color == color

      land_pos = [jump_pos[0] + dx, jump_pos[1] + dy]

      next unless @board.valid_pos?(land_pos)
      next unless @board.empty?(land_pos)
      moves << land_pos
    end

    moves
  end

  def perform_slide(to_pos)
    return false unless slide_moves.include?(to_pos) # TODO raise error?

    @board.remove_piece(pos)
    @board.add_piece(to_pos, self)
    self.pos = to_pos
    maybe_promote
    true
  end

  def perform_jump(to_pos)
    return false unless jump_moves.include?(to_pos) # TODO raise custom error?

    # jumped pos = average of rows and average of cols
    jumped_pos = [(pos[0] + to_pos[0]) / 2.0, (pos[1] + to_pos[1]) / 2.0]

    @board.remove_piece(pos)
    @board.add_piece(to_pos, self)
    self.pos = to_pos
    @board.remove_piece(jumped_pos)
    maybe_promote
    true
  end

  def valid_move_seq?(move_sequence)
    board_copy = @board.dup
    piece_copy = board_copy[self.pos]

    piece_copy.perform_moves!(move_sequence)
  end

  def perform_moves(move_sequence)
    if valid_move_seq?(move_sequence)
      perform_moves!(move_sequence)
    else
      InvalidMovesError.new("Invalid move sequence!")
    end
  end

  def perform_moves!(move_sequence)
    if move_sequence.count == 1
      perform_single_move!(move_sequence)
    else
      perform_multiple_moves!(move_sequence)
    end
  end

  def perform_single_move!(move_sequence)
    raise "Can only perform a single move" if move_sequence.count > 1

    to_pos = move_sequence.first
    unless perform_slide(to_pos)
      return false unless perform_jump(to_pos)
    end

    true
  end

  def perform_multiple_moves!(move_sequence)
    raise "Should have multiple moves" if move_sequence.count < 2

    move_sequence.each do |to_pos|
      return false unless perform_jump(to_pos)
    end

    true
  end

  def maybe_promote
    row = pos.first
    promote! if row == crowned_row
  end

  def promote!
    @king = true
  end

  def inspect
    "pos: #{pos}, color: #{color}"
  end
end
