# encoding: utf-8
require_relative './piece.rb'
require_relative './exceptions.rb'

class Board
  attr_reader :size
  attr_accessor :grid

  def initialize(size=8, place_pieces=true)
    @size = size # TODO constantize?

    build_board(place_pieces)
  end

  def add_piece(pos, piece)
    self[pos] = piece
  end

  def remove_piece(pos)
    self[pos] = nil
  end

  def [](pos)
    raise InvalidMovesError.new("Move outside of board") unless valid_pos?(pos)

    x, y = pos

    @grid[x][y]
  end

  def []=(pos, piece)
    raise InvalidMovesError.new("Move outside of board") unless valid_pos?(pos)

    x, y = pos

    @grid[x][y] = piece
  end

  def valid_pos?(pos)
    x, y = pos

    x >= 0 && x < size && y >= 0 && y < size
  end

  def empty?(pos)
    raise InvalidMovesError.new("Move outside of board") unless valid_pos?(pos)

    self[pos].nil?
  end

  def pieces
    grid.flatten.compact
  end

  def render
    new_str = "\n   " << ('0'...'8').to_a.join("  ") << "\n"

    grid.each_with_index do |row, row_idx|
      new_str << row_idx.to_s << " "
      row.each_with_index do |piece, col_idx|
        if (row_idx.even? && col_idx.even?) || (row_idx.odd? && col_idx.odd?)
          if piece.nil?
            new_str << ("   ").colorize(:background => :light_white)
          else
            new_str << (" " + piece.render + " ").colorize(:background => :light_white)
          end
        else
          if piece.nil?
            new_str << ("   ").colorize(:background => :light_black)
          else
            new_str << ("  " + piece.render + " ").colorize(:background => :light_black)
          end
        end
      end

      new_str << "\n"
    end

    new_str
  end

  def display
    puts render
  end

  def build_board(place_pieces)
    @grid = Array.new(size) { Array.new(size) }
    return if place_pieces == false

    # build red (top)
    (0..2).each do |row| # dry this up
      @grid[row].length.times do |col|
        if row.even? && col.even? || row.odd? && col.odd?
          Piece.new(:red, [row, col], self)
        end
      end
    end

    (5..7).each do |row|
      @grid[row].length.times do |col|
        if row.odd? && col.odd? || row.even? && col.even?
          Piece.new(:black, [row, col], self)
        end
      end
    end
  end

  def dup
    copy = self.class.new(size, false)
    pieces.each do |piece|
      piece.class.new(piece.color, piece.pos, copy)
    end

    copy
  end

  def inspect
    render
  end

  def over?
    pieces.count == 1
  end

  def winner
    if over?
      pieces.first.color
    end

    nil
  end
end


# $b = Board.new
# $b.display
# $piece = $b[[2,2]]
# $piece.perform_slide([3,1])
# $piece.perform_slide([4,2])
# $pp = $b[[1,3]]
# $pp.perform_slide([2,2])
# $bbb = $b[[5,3]]
# $b.display
# $bbb.perform_moves!([[3,1],[1,3]])
# $b.display
# $rrr = $b[[1,1]]
# $rrr.perform_slide([2,2])
# $rr = $b[[0,2]]
# $rr.perform_slide([1,1])
# $b.display

#
